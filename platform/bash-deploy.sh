#!/usr/bin/env bash

aws s3 cp dist $1 --recursive --exclude="*" --include="index.bundle.js" --include="index.bundle.css"
