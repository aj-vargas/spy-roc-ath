const fs = require('fs');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

// The following IIFE filters the timezone data of moment-timezone because the
// app only needs 'America/New_York' and the complete file is around 1 MiB.
(function () {
  const wanted = [
    'America/New_York'
  ];
  const contents = fs.readFileSync(
    'node_modules/moment-timezone/data/packed/latest.json',
    {encoding: 'utf8'}
  );
  const tz = JSON.parse(contents);
  tz.links = [];
  tz.zones = tz.zones.filter(zone => {
    for (const want of wanted) {
      if (zone.startsWith(want)) {
        return true;
      }
    }
    return false;
  });
  fs.writeFileSync('./src/filtered-tz-data.json', JSON.stringify(tz));
})();

module.exports = (env, argv) => ({
  entry: {
    index: './src/index.js'
  },
  output: {
    filename: '[name].bundle.js',
    path: path.join(__dirname, 'dist')
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: [
          {loader: 'vue-loader'}
        ]
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: argv.mode === 'production' ?
              MiniCssExtractPlugin.loader : 'vue-style-loader'
          },
          {
            loader: 'css-loader',
            options: {modules: true, localIdentName: '[local]'}
          }
        ]
      }
    ]
  },
  plugins: [
    new MomentLocalesPlugin(), // Needed to discard useless locales: https://github.com/moment/moment/issues/1435
    new VueLoaderPlugin(),
    new MiniCssExtractPlugin({filename: '[name].bundle.css'}),
    new OptimizeCssAssetsPlugin()
  ]
});
