/* global document io */

import Vue from 'vue';
import TickerGraph from './ticker-graph';
import SpyRocAth from './spy-roc-ath.vue';

const TICKS_LIMIT = 390; // Total minutes in daily market hours: 6 * 60 + 30
const SpyRocAthConstructor = Vue.extend(SpyRocAth);

document.addEventListener('DOMContentLoaded', () => {
  if (typeof io === 'undefined') {
    return;
  }
  const component = new SpyRocAthConstructor({el: 'vue-app'});
  const graph = new TickerGraph('ticker-graph', TICKS_LIMIT);
  const socket = io('https://io.aj-vargas.me/spy-roc-ath');
  let ticksPerInterval = 0;
  socket.on('snapshot', snapshot => {
    component.athPrice = snapshot.ath.price;
    component.athTime = snapshot.ath.time;
    component.marketStatusName = snapshot.marketStatus.status.NAME;
    component.marketStatusTime = snapshot.marketStatus.time;
    component.ticks.splice(0);
    component.ticks.unshift(...snapshot.ticks.splice(0, TICKS_LIMIT));
    component.articles.splice(0);
    component.articles.unshift(...snapshot.articles);
    component.roc = snapshot.roc;
    graph.bind(component.ticks);
  });
  socket.on('articles', articles => {
    component.articles.unshift(...articles);
  });
  socket.on('tick', tick => {
    component.athPrice = tick.ath.price;
    component.athTime = tick.ath.time;
    component.roc = tick.roc;
    component.ticks.unshift(tick.tick);
    ++ticksPerInterval;
  });
  setInterval(() => {
    component.clock = new Date();
    graph.update(ticksPerInterval);
    if (component.ticks.length > TICKS_LIMIT) {
      component.ticks.splice(-ticksPerInterval);
    }
    ticksPerInterval = 0;
  }, 1000);
});
