/* global getComputedStyle */

const anime = require('animejs');
const moment = require('moment-timezone/moment-timezone');

const MOMENT_FORMAT_LONG = 'YYYY-MM-DD HH:mm:ss';
const MOMENT_FORMAT_CLOCK = 'HH:mm:ss';

let MOVEMENT_COLOR_UP = null;
let MOVEMENT_COLOR_DOWN = null;
let MOVEMENT_COLOR_DEFAULT = null;

moment.tz.load(require('./filtered-tz-data.json')); /* eslint-disable-line import/no-unresolved */

export default {
  name: 'SpyRocAth',
  data() {
    return {
      athPrice: 0,
      athTime: null,
      marketStatusName: '',
      marketStatusTime: null,
      ticks: [],
      articles: [],
      roc: 0,
      clock: new Date(),
      animated: {
        roc: 0,
        lastPrice: 0,
        athPrice: 0
      }
    };
  },
  mounted() {
    MOVEMENT_COLOR_UP =
      getComputedStyle(this.$refs.container).getPropertyValue('--green').trim();
    MOVEMENT_COLOR_DOWN =
      getComputedStyle(this.$refs.container).getPropertyValue('--red').trim();
    MOVEMENT_COLOR_DEFAULT =
      getComputedStyle(this.$refs.container).getPropertyValue('--white').trim();
  },
  computed: {
    animatedRoc() {
      return this.animated.roc.toFixed(3);
    },
    animatedLastPrice() {
      return this.animated.lastPrice.toFixed(3);
    },
    animatedAthPrice() {
      return this.animated.athPrice.toFixed(3);
    },
    computedAthTime() {
      if (!this.athTime) {
        return;
      }
      return moment
        .utc(this.athTime)
        .tz('America/New_York')
        .format(MOMENT_FORMAT_LONG);
    },
    computedLastTime() {
      if (!this.ticks.length) {
        return;
      }
      return moment
        .utc(this.ticks[0].time)
        .tz('America/New_York')
        .format(MOMENT_FORMAT_LONG);
    },
    computedMarketStatusTime() {
      if (!this.marketStatusTime) {
        return;
      }
      return moment
        .utc(this.marketStatusTime)
        .tz('America/New_York')
        .format(MOMENT_FORMAT_LONG);
    },
    computedMarketStatusName() {
      switch (this.marketStatusName) {
        case 'Open':
          return 'OPEN';
        case 'Close':
          return 'CLOSED';
        default:
          return 'UNKNOWN';
      }
    },
    computedNewYorkTime() {
      return moment(this.clock)
        .tz('America/New_York')
        .format(MOMENT_FORMAT_CLOCK);
    },
    computedMarketMessage() {
      if (this.marketStatusName === 'Open') {
        return 'Live Prices';
      }
      return 'Showing Last Close';
    }
  },
  watch: {
    roc(newValue, oldValue) {
      anime({
        targets: this.animated,
        roc: newValue * 100,
        easing: 'linear'
      });
      this.colorifyMovement(newValue, oldValue, '.spy-roc-ath__roc');
    },
    ticks() {
      if (this.ticks.length) {
        anime({
          targets: this.animated,
          lastPrice: this.ticks[0].price,
          easing: 'linear'
        });
        const lastPrice = this.ticks[0].price;
        const beforeLastPrice = this.ticks.length > 1 ? this.ticks[1].price : 0;
        this.colorifyMovement(lastPrice, beforeLastPrice, '.spy-roc-ath__last');
      }
    },
    athPrice(newValue, oldValue) {
      anime({
        targets: this.animated,
        athPrice: newValue,
        easing: 'linear'
      });
      this.colorifyMovement(newValue, oldValue, '.spy-roc-ath__ath');
    }
  },
  methods: {
    formatMomentLong(time) {
      return moment.utc(time).tz('America/New_York').format(MOMENT_FORMAT_LONG);
    },
    colorifyMovement(newValue, oldValue, selector) {
      let color = null;
      if (newValue > oldValue) {
        color = MOVEMENT_COLOR_UP;
      } else if (newValue < oldValue) {
        color = MOVEMENT_COLOR_DOWN;
      }
      if (color) {
        anime({
          targets: selector,
          color: [
            {value: MOVEMENT_COLOR_DEFAULT, duration: 0},
            {value: color, delay: 0, duration: 500},
            {value: MOVEMENT_COLOR_DEFAULT, delay: 500, duration: 500}
          ],
          easing: 'linear'
        });
      }
    }
  }
};
