/* eslint import/no-extraneous-dependencies: 0 */

const d3 = Object.assign(
  {},
  require('d3-array'),
  require('d3-scale'),
  require('d3-shape'),
  require('d3-selection'),
  require('d3-ease'),
  require('d3-transition')
);

class TickerGraph {
  constructor(selectorName, ticksLimit) {
    this.selectorName = selectorName;
    this.ticksLimit = ticksLimit;
    this.viewport = d3.select(`.${this.selectorName}__viewport`);
    this.graphic = d3.select(`.${this.selectorName}__graphic`);
    this.bounds = d3.select(`.${this.selectorName}__bounds`);
    this.xScale = d3.scaleLinear()
      .domain([this.ticksLimit, 0]);
    this.yScale = d3.scaleLinear();
    this.line = d3.line()
      .defined(d => !!d.price)
      .x((d, i) => this.xScale(i))
      .y(d => this.yScale(d.price))
      .curve(d3.curveBasis);
    this.path = this.graphic
    .append('path') /* eslint-disable-line indent */
      .attr('class', `${this.selectorName}__path`);
    this.dimensionalize();
    this.xScaleRangeUnit = this.xScale(this.ticksLimit - 1);
  }

  dimensionalize() {
    const viewportWidth = +this.viewport.style('width').replace('px', '');
    const viewportHeight = +this.viewport.style('height').replace('px', '');
    this.viewport.attr('viewBox', `0 0 ${viewportWidth} ${viewportHeight}`);
    this.margin = {
      top: +this.graphic.style('margin-top').replace('px', ''),
      right: +this.graphic.style('margin-right').replace('px', ''),
      bottom: +this.graphic.style('margin-bottom').replace('px', ''),
      left: +this.graphic.style('margin-left').replace('px', '')
    };
    this.width = viewportWidth - this.margin.left - this.margin.right;
    this.height = viewportHeight - this.margin.top - this.margin.bottom;
    this.graphic.attr(
      'transform',
      'translate(' + this.margin.left + ', ' + this.margin.top + ')'
    );
    this.bounds
      .attr('width', this.width)
      .attr('height', this.height);
    this.xScale.range([0, this.width]);
    this.yScale.range([this.height, 0]);
  }

  bind(data) {
    if (data.length) {
      this.yScale.domain(d3.extent(data, d => d.price));
    }
    this.path
      .datum(null)
      .datum(data)
      .attr('opacity', 0)
      .attr('d', this.line)
    .transition() /* eslint-disable-line indent */
      .duration(700)
      .ease(d3.easeLinear)
      .attr('opacity', 1);
  }

  update(offscreenTicks) {
    if (!offscreenTicks) {
      return;
    }
    const yDomain = this.yScale.domain();
    const yExtent = d3.extent(this.path.datum(), d => d.price);
    this.xScale.range([
      0, this.xScaleRangeUnit * (this.ticksLimit + offscreenTicks)
    ]);
    if (yExtent[0] < yDomain[0] || yExtent[1] > yDomain[1]) {
      const yScaleTranslationDelay = 200;
      this.yScale.domain(yExtent);
      this.path
      .transition() /* eslint-disable-line indent */
        .duration(yScaleTranslationDelay / 2)
        .ease(d3.easeLinear)
        .attr('opacity', 0)
        .on('end', () => {
          this.path
            .attr('d', this.line)
            .attr('transform', null);
        })
      .transition() /* eslint-disable-line indent */
        .delay(yScaleTranslationDelay / 2)
        .duration(yScaleTranslationDelay / 2)
        .ease(d3.easeLinear)
        .attr('opacity', 1)
      .transition() /* eslint-disable-line indent */
        .delay(yScaleTranslationDelay)
        .duration(300)
        .ease(d3.easeLinear)
        .attr('opacity', 1)
        .attr(
          'transform',
          'translate(' + (-1 * this.xScaleRangeUnit * offscreenTicks) + ', 0)'
        );
    } else {
      this.path
        .attr('d', this.line)
        .attr('transform', null)
      .transition() /* eslint-disable-line indent */
        .duration(300)
        .ease(d3.easeLinear)
        .attr('opacity', 1)
        .attr(
          'transform',
          'translate(' + (-1 * this.xScaleRangeUnit * offscreenTicks) + ', 0)'
        );
    }
  }

  /**
   * @deprecated Resizing is done automatically by the viewport/viewbox ratios.
   * @see https://www.sarasoueidan.com/blog/svg-coordinate-systems/
   */
  resize() {
    this.dimensionalize();
    this.path.attr('d', this.line);
  }
}

export default TickerGraph;
