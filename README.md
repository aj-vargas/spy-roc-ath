# spy-roc-ath

A web app that shows the rate of change (ROC) since the last all time high (ATH) of the SPDR S&P 500 ETF (SPY) in the IEX exchange.

Other indicators shown are the last price, the last ATH, the market status of the instrument, news related to the SPY ticker, and a graph showing either the last closing prices or near real-time prices (when its market is open).

A deployed instance of this app can be seen [here](https://www.aj-vargas.me/spy-roc-ath).

Data provided for free by [IEX](https://iextrading.com/developer). View [IEX's Terms of Use](https://iextrading.com/api-exhibit-a/).
